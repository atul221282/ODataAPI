﻿using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using WebApplication1.Models;
using System.Web.Http;

namespace WebApplication1
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<Product>("Products");
            builder.EntitySet<ProductCategory>("ProductCategory");
            builder.EntitySet<ProductTags>("ProductTags");
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
