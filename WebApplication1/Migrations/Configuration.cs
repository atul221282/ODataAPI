namespace WebApplication1.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebApplication1.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ProductsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProductsContext context)
        {
            context.Products.AddOrUpdate(new Product[]
            {
                new Product() { ID = 1, Name = "Hat", Price = 15, ProductCategory = new ProductCategory
                    {
                        ID=1,
                        Name= "Apparel"
                    }
                },
                new Product() { ID = 2, Name = "Socks", Price = 5, ProductCategory = new ProductCategory
                    {
                        ID=1,
                        Name= "Apparel"
                    }
                },
                new Product() { ID = 3, Name = "Scarf", Price = 12, ProductCategory = new ProductCategory
                    {
                        ID=1,
                        Name= "Apparel"
                    },
                    ProductTags= new List<ProductTags>
                    {
                        new ProductTags{ ID=1,TagName="Clothing"}
                    }
                },
                new Product() { ID = 4, Name = "Yo-yo", Price = 4.95M, ProductCategory = new ProductCategory
                    {
                        ID=1,
                        Name= "Toys"
                    }
                },
                new Product() { ID = 5, Name = "Puzzle", Price = 8, ProductCategory = new ProductCategory
                    {
                        ID=1,
                        Name= "Toys"
                    }
                },
            });
        }
    }
}
