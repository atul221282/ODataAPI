﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int ProductCategoryId { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
        public virtual ICollection<ProductTags> ProductTags { get; set; }
    }

    public class ProductCategory
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class ProductTags
    {
        public int ID { get; set; }
        public string TagName { get; set; }
    }
}